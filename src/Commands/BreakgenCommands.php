<?php

namespace Drupal\breakgen\Commands;

use Drush\Commands\DrushCommands;
use Drupal\breakgen\Service\ImageStyleGenerator;

/**
 * Class BreakgenCommands.
 *
 * @package Drupal\breakgen\Commands
 */
class BreakgenCommands extends DrushCommands {

  /**
   * The generator class for generating the image styles.
   *
   * @var \Drupal\breakgen\Service\ImageStyleGenerator
   */
  protected $generator;

  /**
   * BreakgenCommands constructor.
   *
   * @param \Drupal\breakgen\Service\ImageStyleGenerator $generator
   *   The generator class for generating the image styles.
   */
  public function __construct(ImageStyleGenerator $generator) {
    $this->generator = $generator;
  }

  /**
   * Generates breakgen breakpoints for Drupal 8 out of the theme file.
   *
   * @param string|null $theme
   *   The theme to use for generating the breakpoints.
   *
   * @command breakgen:generate
   * @validate-module-enabled breakgen
   * @aliases bg:generate, bg, breakgen-generate
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generate($theme = NULL) {
    $this->generator->generate($theme);
  }

  /**
   * Clears breakgen breakpoints for Drupal 8 out of the theme file.
   *
   * @command breakgen:clean
   * @validate-module-enabled breakgen
   * @aliases bg:clean, bc, breakgen-clean
   */
  public function clear() {
    $this->generator->clear();
  }

}
